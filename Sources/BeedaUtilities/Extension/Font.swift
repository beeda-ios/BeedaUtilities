// 
// Beeda Seller
//
// Created by Rakibur Khan on 19/4/23.
// Copyright © 2023 Beeda Inc. All rights reserved.
//

import SwiftUI

public extension Font {
    enum PoppinsFont {
        case light
        case medium
        case regular
        case semibold
        
        var name: String {
            var font: String = ""
            
            switch self {
                case .light:
                    font = "Poppins-Light"
                case .medium:
                    font = "Poppins-Medium"
                case .regular:
                    font = "Poppins-Regular"
                case .semibold:
                    font = "Poppins-SemiBold"
            }
            
            return font
        }
    }
    
    static func poppins(forTextStyle textStyle: TextStyle, type: PoppinsFont, fontSize: CGFloat) -> Font {
        if #available(iOS 14.0, *) {
            return .custom(type.name, size: fontSize, relativeTo: textStyle)
        } else {
            return .custom(type.name, size: fontSize)
        }
    }
    
    static func poppins(type: PoppinsFont, fontSize: CGFloat) -> Font {
        return .custom(type.name, size: fontSize)
    }
}
