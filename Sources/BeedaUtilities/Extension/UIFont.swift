// 
// Beeda Seller
//
// Created by Rakibur Khan on 19/4/23.
// Copyright © 2023 Beeda Inc. All rights reserved.
//

import UIKit

public extension UIFont {
    enum PoppinsFont {
        case light
        case medium
        case regular
        case semibold
        
        var name: String {
            var font: String = ""
            
            switch self {
                case .light:
                    font = "Poppins-Light"
                case .medium:
                    font = "Poppins-Medium"
                case .regular:
                    font = "Poppins-Regular"
                case .semibold:
                    font = "Poppins-SemiBold"
            }
            
            return font
        }
    }
    
    final class func poppins(forTextStyle textStyle: UIFont.TextStyle, type: PoppinsFont, fontSize: CGFloat) -> UIFont {
        guard
            let font = UIFont(name: type.name, size: fontSize)
        else {
            return UIFont.preferredFont(forTextStyle: textStyle)
        }
        
        let fontMetrics = UIFontMetrics(forTextStyle: textStyle)
        return fontMetrics.scaledFont(for: font)
    }
    
    final class func poppins(type: PoppinsFont, fontSize: CGFloat) -> UIFont? {
        UIFont(name: type.name, size: fontSize)
    }
}
