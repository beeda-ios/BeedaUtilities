// 
// Beeda Driver
//
// Created by Rakibur Khan on 27/10/22.
// Copyright © 2022 Beeda Inc. All rights reserved.
//

import UIKit
import SVGKit

public extension UIImageView {
    func load(url: URL?, placeholder: UIImage? = UIImage(named: "placeholder"), cache: ImageRepository? = nil) {
        var repository: ImageRepository?
        
        if let cache = cache {
            repository = cache
            
        } else {
            repository = ImageRepository.shared
        }
        var gif: Bool = false
        var svg: Bool = false
        
        if url?.pathExtension == "gif" {
            gif = true
        } else {
            gif = false
        }
        
        if url?.pathExtension == "svg"{
            svg = true
        }else {
            svg = false
        }
        
        Task {
            if gif {
                let imageData = await repository?.getData(imageURL: url)
                if let data = imageData {
                    if let image = UIImage.gifImageWithData(data) {
                        await MainActor.run {
                            self.image = image
                        }
                    } else {
                        self.image = placeholder
                    }
                } else {
                    self.image = placeholder
                }
            } else {
                
                if svg {
                    let imageData = await repository?.getData(imageURL: url)
                    
                    if let data = imageData {
                        if let anSVGImage: SVGKImage = SVGKImage(data: data) {
                            await MainActor.run {
                                self.image = anSVGImage.uiImage
                            }
                        }
                        else {
                            self.image = placeholder
                        }
                    } else {
                        self.image = placeholder
                    }
                    
                }else {
                    
                    let image = await repository?.getImage(imageURL: url)
                    if let image = image {
                        await MainActor.run{
                            self.image = image
                        }
                    } else {
                        self.image = placeholder
                    }
                }
            }
        }
    }
}
